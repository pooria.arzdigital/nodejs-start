let mysql = require('mysql')
const dotenv = require('dotenv')
dotenv.config()

let con = mysql.createConnection({
    host: "localhost",
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
})

con.connect(function (err) {
    if (err) throw err
    console.log("Connected!")
})