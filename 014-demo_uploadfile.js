let http = require('http')
let formidable = require('formidable')
let fs = require('fs')
const dotenv = require('dotenv');
dotenv.config();

http.createServer(function (req, res) {
    if (req.url === '/fileupload') {
        let form = new formidable.IncomingForm()
        form.parse(req, function (err, fields, files) {
            let oldpath = files.filetoupload.filepath

            /*
            * in new path you should use your own username, can be different on windows.
            *
            * */


            let newpath = process.env.UPLOAD_FILE_DIR + files.filetoupload.originalFilename
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err
                res.write('File uploaded and moved!')
                res.end()
            })
        })
    } else {
        res.writeHead(200, {'Content-Type': 'text/html'})
        res.write('<form action="fileupload" method="post" enctype="multipart/form-data">')
        res.write('<input type="file" name="filetoupload"><br>')
        res.write('<input type="submit">')
        res.write('</form>')
        return res.end()
    }
}).listen(8080)