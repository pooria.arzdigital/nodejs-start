var mysql = require('mysql')
var dotenv = require('dotenv')
dotenv.config()

var con = mysql.createConnection({
    host: "localhost",
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    con.query("CREATE DATABASE nodejstest-mydb", function (err, result) {
        if (err) throw err;
        console.log("Database created");
    });
});